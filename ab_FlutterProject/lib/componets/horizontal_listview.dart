import 'package:aa_flutter_app/Lists/aa_PVarieties/VarietiesPackage.dart';
import 'package:flutter/material.dart';

import '../Config.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[



          RaisedButton(
            child: Image.asset('images/cats/aa_tshirt.png',fit: BoxFit.contain),
            onPressed: (){

              Config.PRODUCT_TYPE=2;
              Config.PRODUCT_NAME = "All Shirts";

              Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new  VarietiesPackage()));

            },


          ),
          RaisedButton(
            child: Image.asset('images/cats/ab_dress.png',fit: BoxFit.contain),
            onPressed: (){


              Config.PRODUCT_TYPE=3;
              Config.PRODUCT_NAME = "All Frocks";

              Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new  VarietiesPackage()));


            },


          ),


          RaisedButton(
            child: Image.asset('images/cats/ac_short.png',fit: BoxFit.contain),
            onPressed: (){

              Config.PRODUCT_TYPE=4;
              Config.PRODUCT_NAME = "All pants";
              Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new  VarietiesPackage()));


            },


          ),


          RaisedButton(
            child: Image.asset('images/cats/ad_formal.png',fit: BoxFit.contain),
            onPressed: (){

              Config.PRODUCT_TYPE=5;
              Config.PRODUCT_NAME = "All Formals";
              Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new  VarietiesPackage()));


            },


          ),

          RaisedButton(
            child: Image.asset('images/cats/aa_tshirt.png',fit: BoxFit.contain),
            onPressed: (){

              Config.PRODUCT_TYPE=2;
              Config.PRODUCT_NAME = "All Shirts";

              Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new  VarietiesPackage()));

            },


          ),
          RaisedButton(
            child: Image.asset('images/cats/ab_dress.png',fit: BoxFit.contain),
            onPressed: (){


              Config.PRODUCT_TYPE=3;
              Config.PRODUCT_NAME = "All Frocks";

              Navigator.of(context).push(new MaterialPageRoute(builder: (context)=> new  VarietiesPackage()));


            },


          ),







        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({this.image_location, this.image_caption});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {


        },
        child: Container(
          width: 100.0,
          child: ListTile(
            title: Image.asset(
              image_location,
              width: 80.0,
              height: 60.0,
            ),
            subtitle: Container(
              alignment: Alignment.topCenter,
              child: Text(image_caption, style: new TextStyle(fontSize: 12.0),),
            )
          ),
        ),
      ),
    );
  }
}
