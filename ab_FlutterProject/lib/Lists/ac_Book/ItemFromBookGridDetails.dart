import 'dart:convert';
import 'dart:math';

import 'package:aa_flutter_app/Config.dart';
import 'package:flutter/material.dart';
import 'package:aa_flutter_app/aa_HomeScreenTour.dart';

import 'package:aa_flutter_app/Login/AdminPage.dart';
import 'package:aa_flutter_app/Login/MemberPage.dart';
import 'package:aa_flutter_app/Config.dart';
import 'package:http/http.dart' as http;

class ItemFromBookGridDetails extends StatefulWidget {

  final cartId;
  final prod_name;

  final prod_pricture;
  final prod_old_price;
  final prod_price;
  final prod_id;
  final size;
  final details;
  final description;

  @override
  _ItemFromBookGridDetailsState createState() => _ItemFromBookGridDetailsState();

  ItemFromBookGridDetails(this.cartId,
      this.prod_name, this.prod_pricture, this.prod_old_price, this.prod_price,this.prod_id,this.size,this.details,this.description);
}

class _ItemFromBookGridDetailsState extends State<ItemFromBookGridDetails> {





  Future<String> book_item() async
  {


   print( "Book Check :: "+Config.Id.toString()+"::"+widget.prod_id.toString()+"::"+ widget.cartId.toString());

   print("Book Check :: "+Config.url_book);
    final response = await http.post(Uri.parse(Config.url_book), body: {

      "user_id": Config.Id.toString(),
      "package_id":widget.prod_id.toString() ,
      "cart_id"   : widget.cartId.toString()
    }, headers: {"Accept": "application/json"});




    setState(()
    {
       var resBody = json.decode(response.body.toString());
       Navigator.pushReplacementNamed(context, '/TourHome');

       print( "Book Check :: "+resBody.toString());


    });






    return "Success!";
  }





  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.green,
        title: new Text("Apparel Shop"),
        centerTitle: true,
        elevation: 10.0,
        actions: <Widget>[



        ],
      ),
      body: new ListView(
        children: <Widget>[
          //image carousel begins here

          Container(
            height: 180,
            child: GridTile(
              child: Container(
                  color: Colors.white,
                 // child: Image.asset(widget.prod_pricture)
                //  Image.network(prod_pricture, fit: BoxFit.cover)
                  child: Image.network(widget.prod_pricture, fit: BoxFit.contain)
              ),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  leading: new Text(
                    widget.prod_name,
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.00),
                  ),
                  title: new Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("\$${widget.prod_old_price}",
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.w800,
                              decoration
                                  :TextDecoration.lineThrough),
                        ),


                      ),
                      Expanded(
                        child: new Text("\$${widget.prod_price}",
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.w800,
                          ),),
                      ),


                    ],
                  ),
                ),
              ),
            ),
          ),


          Row(
            children: <Widget>[


              Expanded(
                child: MaterialButton(
                  onPressed: () {

                    book_item();
                    //Navigator.pushReplacementNamed(context, '/TourHome');
                  },
                  color: Colors.red,
                  textColor: Colors.white,
                  elevation: 2.0,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: new Text("Buy now",textAlign: TextAlign.center,),
                      ),

                    ],
                  ),
                ),
              ),


            ],
          ),
          Divider(),
          new ListTile(
            title: new Text("Item Details",textAlign: TextAlign.justify,style: TextStyle(fontWeight: FontWeight.bold)),
            subtitle: new Text(widget.details,),
          ),
          Divider(),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Item Name",textAlign: TextAlign.justify,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text(widget.prod_name,textAlign: TextAlign.center,style: TextStyle(color: Colors.grey)))

            ],
          )
          ,
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Price Price",textAlign: TextAlign.justify,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text("\$${widget.prod_price}",textAlign: TextAlign.justify,style: TextStyle(color: Colors.grey)))


            ],
          ),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Old Price",textAlign: TextAlign.justify,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text(widget.prod_old_price.toString(),textAlign: TextAlign.justify,style: TextStyle(color: Colors.grey)))


            ],
          )
          , new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("Details",style: TextStyle(color: Colors.grey))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text(widget.details.toString(),style: TextStyle(color: Colors.grey)))


            ],
          ),



          new Row(children: <Widget>[
            new Flexible(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                      child: new Text("Description",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold))),
                  Padding(padding: const EdgeInsets.all(5),
                      child: new Text(widget.description.toString(),style: TextStyle(color: Colors.grey),  textAlign: TextAlign.justify))
                ],
              ),
            ),
          ]),
          new Row(
            children: <Widget>[
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
                  child: new Text("size",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold))),
              Padding(padding: const EdgeInsets.all(5),
                  child: new Text(widget.size.toString(), textAlign: TextAlign.justify,style: TextStyle(color: Colors.grey)))


            ],
          )
        ],
      ),

    );
  }



}


