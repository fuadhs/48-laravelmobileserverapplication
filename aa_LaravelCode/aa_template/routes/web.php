<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return "Server Working";
});

Route::get('/adminLogin',"aa_Mobile\zd_AdminPage\AdminController@admin_db");
Route::post('/adminLogin',"aa_Mobile\zd_AdminPage\AdminController@admin_db_post");
Route::get('/adminUpload',"aa_Mobile\zd_AdminPage\AdminController@admin_db_upload_get")->name('gotoupload');
Route::post('/adminUpload',"aa_Mobile\zd_AdminPage\AdminController@admin_upload_data");
Route::get('/wrongpassword',"aa_Mobile\zd_AdminPage\AdminController@admin_wrong_password");

//Login in to the System(
Route::post('/aa_user_login',"aa_Mobile\za_Login_Register\UserAccessController@login_db");


//Home Screen
Route::post('/_url_home_page_carousel_random',"aa_Mobile\zb_HomeScreen\HomeScreenController@get_random_carousel_apparel_images");
Route::post('/_url_home_page_product_varieties',"aa_Mobile\zb_HomeScreen\HomeScreenController@productVarieties");



//Drawer
Route::post('/_url_add_cart_items_by_id',"aa_Mobile\zc_Book_and_Cart\CartController@AddToCart");
Route::post('/_url_display_cart_items_by_id',"aa_Mobile\zc_Book_and_Cart\CartController@ShowAllCartById");


//Drawer
Route::post('/_url_add_book_items_by_id',"aa_Mobile\zc_Book_and_Cart\BookController@AddToBook");
Route::post('/_url_display_book_items_by_id',"aa_Mobile\zc_Book_and_Cart\BookController@ShowAllBookById");
Route::post('/_url_display_product_items_by_id',"aa_Mobile\zc_Book_and_Cart\ProductTypeController@ShowAllProductById");
//Route::post('/_url_home_page_product_random',"aa_Mobile\zb_HomeScreen\HomeScreenController@random_view_package");

//AddToCart
/*

//Home Page
Route::post('/url_view_random_packages',"ab_Apparel\aa_Apparel@get_random_carousel_apparel_image");

*/
