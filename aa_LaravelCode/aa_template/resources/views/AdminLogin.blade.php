<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-heading">
            <h2 class="text-center">Admin Login</h2>
        </div>
        <hr />
        <div class="modal-body">
            <form action="/adminLogin" role="form" method="post">
                <div class="form-group">
                    <div class="input-group">
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
							</span>
                        <input type="email" class="form-control" name="email" placeholder="email" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
							</span>
                        <input type="password" name="password" class="form-control" placeholder="Password" />

                    </div>

                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success btn-lg text-center">Login</button>

                </div>

            </form>
        </div>
    </div>
</div>









</body>
</html>
